$( document ).ready(function() {
    
	if($('#registerTab').length)
		SelectTab("registerTab");
	if($('#dataTab').length)
		SelectTab("dataTab");
	
	$(".tab").click(function(){
	  var clickedID = $(this).attr('id');
	  SelectTab(clickedID);
	});
});

function SelectTab(clickedID) {
	$("#tabs").find(".selected").removeClass("selected");
	$("#" + clickedID).addClass("selected");
	
	if(clickedID == "registerTab") {
		$( "#dynamicContainer" ).load( "ajax/register.html" );
	}
	else if(clickedID == "loginTab") {
		$( "#dynamicContainer" ).load( "ajax/login.html" );
	}
	else if(clickedID == "recoverTab") {
		$( "#dynamicContainer" ).load( "ajax/recover.html" );
	}
	else if(clickedID == "dataTab") {
		$( "#dynamicContainer" ).load( "ajax/accountData.php" );
	}
	else if(clickedID == "downloadTab") {
		$( "#dynamicContainer" ).load( "ajax/downloadClient.html" );
	}
	else if(clickedID == "changePassTab") {
		$( "#dynamicContainer" ).load( "ajax/changePassword.html" );
	}
	else if(clickedID == "changeMailTab") {
		$( "#dynamicContainer" ).load( "ajax/changeMail.html" );
	}
	else if(clickedID == "logoutTab") {
		$( "#dynamicContainer" ).load( "ajax/logout.php" );
	}
	
}

function SendRegister(params) {
	
	var error = false;
	
	$("#formErrors").html("");
	
	if(params[0].value == "") {
		$("#formErrors").append("Username can't be empty.<br>");
		error = true;
		$("#usernameInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#usernameInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(params[1].value == "" || params[1].value != params[2].value) {
		$("#formErrors").append("Password is empty or not match.<br>");
		error = true;
		$("#passwordInput").removeClass("goodForm").addClass("wrongForm");
		$("#password2Input").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#passwordInput").removeClass("wrongForm").addClass("goodForm");
		$("#password2Input").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(params[3].value == "") {
		$("#formErrors").append("Email can't be empty.<br>");
		error = true;
		$("#emailInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#emailInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(!error) {
		$("#registerButton").prop('disabled', true);
		$("#usernameInput").prop('disabled', true);
		$("#passwordInput").prop('disabled', true);
		$("#password2Input").prop('disabled', true);
		$("#emailInput").prop('disabled', true);
		
		$.post(
			"forms/registerForm.php",
			{ user: params[0].value,
			pass: params[1].value,
			email: params[3].value},
			function(response) {
				$("#registerButton").prop('disabled', true);
				$("#usernameInput").prop('disabled', false);
				$("#passwordInput").prop('disabled', false);
				$("#password2Input").prop('disabled', false);
				$("#emailInput").prop('disabled', false);
				if(response!="success")
					$("#formErrors").html(response);
				else {
					window.location.href = "management.php";
				}
			}
		);
	}
}
	
function Login(params) {
	
	var error = false;

	$("#formErrors").html("");
	
	if(params[0].value == "") {
		$("#formErrors").append("Username can't be empty.<br>");
		error = true;
		$("#usernameInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#usernameInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(params[1].value == "") {
		$("#formErrors").append("Password can't be empty.<br>");
		error = true;
		$("#passwordInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#passwordInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(!error) {
		$("#loginButton").prop('disabled', true);
		$("#usernameInput").prop('disabled', true);
		$("#passwordInput").prop('disabled', true);
		
		$.post(
			"forms/loginForm.php",
			{ user: params[0].value,
			pass: params[1].value},
			function(response) {
				$("#loginButton").prop('disabled', false);
				$("#usernameInput").prop('disabled', false);
				$("#passwordInput").prop('disabled', false);
				if(response!="success")
					$("#formErrors").html(response);
				else {
					window.location.href = "management.php";
				}
			}
		);
	}
}

function RecoverPassword(params) {
	
	$("#formErrors").html("");
	
	if(params[0].value == "") {
		$("#formErrors").append("Username can't be empty.<br>");
		$("#usernameInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#usernameInput").prop('disabled', true);
		$("#recoverButton").prop('disabled', true);
		$("#usernameInput").removeClass("wrongForm").addClass("goodForm");
		
		$.post(
			"forms/recoverForm.php",
			{ user: params[0].value},
			function(response) {
				$("#usernameInput").prop('disabled', false);
				$("#recoverButton").prop('disabled', false);
				if(response!="success")
					$("#formErrors").html(response);
			}
		);
	}
}

function ChangePassword(params) {
	
	var error = false;
	
	$("#formErrors").html("");
	
	if(params[0].value == "") {
		$("#formErrors").append("The current password can't be empty.<br>");
		error = true;
		$("#passwordInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#passwordInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(params[1].value == "" || params[1].value != params[2].value) {
		$("#formErrors").append("The new password is empty or not match.<br>");
		error = true;
		$("#password2Input").removeClass("goodForm").addClass("wrongForm");
		$("#password3Input").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#password2Input").removeClass("wrongForm").addClass("goodForm");
		$("#password3Input").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(!error) {
		$("#changePassButton").prop('disabled', true);
		$("#passwordInput").prop('disabled', true);
		$("#password2Input").prop('disabled', true);
		$("#password3Input").prop('disabled', true);
		$.post(
			"forms/changePasswordForm.php",
			{ pass: params[0].value,
			newPass: params[1].value},
			function(response) {
				$("#changePassButton").prop('disabled', false);
				$("#passwordInput").prop('disabled', false);
				$("#password2Input").prop('disabled', false);
				$("#password3Input").prop('disabled', false);
				if(response!="success")
					$("#formErrors").html(response);
				else {
					window.location.href = "/";
				}
			}
		);
	}
}

function ChangeEmail(params) {
	
	var error = false;
	
	$("#formErrors").html("");
	
	if(params[0].value == "" || params[0].value != params[1].value) {
		$("#formErrors").append("The new email is empty or not match.<br>");
		error = true;
		$("#newEmail").removeClass("goodForm").addClass("wrongForm");
		$("#confirmNewEmail").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#newEmail").removeClass("wrongForm").addClass("goodForm");
		$("#confirmNewEmail").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(params[2].value == "") {
		$("#formErrors").append("Your password can' be empty.<br>");
		error = true;
		$("#passwordInput").removeClass("goodForm").addClass("wrongForm");
	}
	else {
		$("#passwordInput").removeClass("wrongForm").addClass("goodForm");
	}
	
	if(!error) {
		$("#changeEmailButton").prop('disabled', true);
		$("#newEmail").prop('disabled', true);
		$("#confirmNewEmail").prop('disabled', true);
		$("#passwordInput").prop('disabled', true);
		$.post(
			"forms/changeEmailForm.php",
			{ newEmail: params[0].value,
			pass: params[2].value},
			function(response) {
				$("#changeEmailButton").prop('disabled', false);
				$("#newEmail").prop('disabled', false);
				$("#confirmNewEmail").prop('disabled', false);
				$("#passwordInput").prop('disabled', false);
				if(response!="success")
					$("#formErrors").html(response);
				else {
					window.location.href = "/";
				}
			}
		);
	}
	
}
