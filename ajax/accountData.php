<?

include ("../forms/database.php");

session_start();

Database::Connect();

$account = Database::GetAccountData($_SESSION['user']);



echo 'Account name: <span class="bold">' . ucfirst(strtolower($_SESSION['user'])) . '</span><br>';
echo 'Account ID: <span class="bold">#' . $account["user_id"] . '</span><br>';
echo 'Email: <span class="bold">' . $account["email"] . '</span><br>';
echo 'Creation date: <span class="bold">' . $account["creation_date"] . '</span><br>';
echo 'Register IP: <span class="bold">' . $account["register_ip"] . '</span><br>';
echo '<br>';
if($account["banned_until"] == null)
	echo 'Your account is active.';
else 
	echo 'Your account is banned until ' . $account["banned_until"] . '.';

?>