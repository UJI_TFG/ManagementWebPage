<? 

if(isset($_POST['email']) && isset($_POST['user']) && isset($_POST['pass'])) {
	
	//Register new user
	include "database.php";
	
	Database::Connect();
	$success = Database::RegisterUser($_POST['user'], $_POST['pass'], $_POST['email']);
	
	if($success) {
		//Send register email
		include ("mailer.php");
		
		Mailer::SendMail($_POST['email'], ucfirst(strtolower($_POST['user'])),
			'Register confirmation',
			"Welcome, " . ucfirst(strtolower($_POST['user'])) . ". You completed the register to TFG Shooter.",
			"Welcome, " . ucfirst(strtolower($_POST['user'])) . ".<br><br>You completed the register to TFG Shooter.<br><br>You can manage your account from the webpage.<br><br>Have a nice day!"
		);
		
		//Login
		session_start();
		$_SESSION['user'] = ucfirst($_POST['user']);
	}
}
else
	echo "ERROR";
	
?>