<?

if(isset($_POST['pass']) && isset($_POST['newPass'])) {
	
	include ("database.php");
	
	Database::Connect();
	
	//Iniciar sesion
	session_start();
	
	//Check if the current password is correct
	if(Database::CheckPasswordMatch($_SESSION['user'], $_POST['pass'])) {
		//Then, update with the new password
		Database::UpdatePassword($_SESSION['user'], $_POST['newPass']);
		
		//And send an email
		include ("mailer.php");
			
		Mailer::SendMail(Database::GetEmail($_SESSION['user']), ucfirst(strtolower($_SESSION['user'])),
			'Password changed',
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ". Password change request success.",
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ".<br>You requested a password change from the management webpage.<br><br>Your new password is not shown on this email for security reasons, store it in a safe place.<br>If you forget it, use the recovery tool on the webpage.<br><br>If you have not requested this operation, contact us replying to this message.<br><br>Have a nice day!"
		);
		
		session_destroy();
		
		echo "success";
	}
	else {
		echo "The current password is not correct.";
	}
}
else
	echo "ERROR";
	
?>