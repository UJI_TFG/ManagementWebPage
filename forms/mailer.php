<?

include ("PHPMailer/class.phpmailer.php");
include ("PHPMailer/class.smtp.php");

class Mailer {

	private static $sender="email@gmail.com";
	private static $senderName="TFG Project";
	private static $password="password";

	public static function SendMail($receiver, $receiverName, $subject, $altBody, $messageHTML) {
		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		
		try {
			//$mail->SMTPDebug = 2;
			$mail->SMTPAuth   = true;                  // Habilita la autenticación SMTP 
			$mail->SMTPSecure = "ssl";                 // Establece el tipo de seguridad SMTP 
			$mail->Host       = "smtp.gmail.com";      // Establece Gmail como el servidor SMTP 
			$mail->Port       = 465;                   // Establece el puerto del servidor SMTP de Gmail 
			$mail->Username   = self::$sender;               // Usuario Gmail 
			$mail->Password   = self::$password;             // Contraseña Gmail 
			
			//A que dirección se puede responder el correo 
			$mail->AddReplyTo(self::$sender, self::$senderName); 
			//La direccion a donde mandamos el correo 
			$mail->AddAddress($receiver, $receiverName); 
			//De parte de quien es el correo 
			$mail->SetFrom(self::$sender, self::$senderName); 
			//Asunto del correo 
			$mail->Subject = $subject; 
			//Mensaje alternativo en caso que el destinatario no pueda abrir correos HTML 
			$mail->AltBody = $altBody; 
			//El cuerpo del mensaje, puede ser con etiquetas HTML 
			$mail->MsgHTML($messageHTML); 
			//Enviamos el correo 
			$mail->Send(); 
		} catch (phpmailerException $e) { 
			echo $e->errorMessage(); //PhpMailer errors
		} catch (Exception $e) { 
			echo $e->getMessage(); //Other errors
		}
	}
}
?>