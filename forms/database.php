<?

class Database {
	
	const HOST = "localhost";
	const PORT = 5432;
	const NAME = "tfg_database";
	const USER = "tfgtest";
	const PASS = "tfgtest";
	
	private static $conn;
	
	public static function Connect() {
		self::$conn = pg_connect("host=".self::HOST." port=".self::PORT." dbname=".self::NAME." user=".self::USER." password=".self::PASS);
    }
	
	public static function CheckIfUsernameExists($username) {
		$sql = 'SELECT user_id FROM users WHERE lower(username) = lower($1)';
		$sqlName = 'checkExistentUsername';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username));
		
		$arr = pg_affected_rows($result);
		return $arr > 0;
	}
	
	public static function RegisterUser($username, $pass, $email) {
		
		$existsUsername = Database::CheckIfUsernameExists($username);
		
		//check email
		$sql = 'SELECT user_id FROM users WHERE lower(email) = lower($1)';
		$sqlName = 'checkExistentEmail';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($email));
		
		$arr = pg_fetch_array($result);
		$existsEmail = $arr > 0;
		
		if($existsUsername)
			echo "The introduced username is already registered.<br>";
		
		if($existsEmail)
			echo "The introduced email is already registered.<br>";

		if(!$existsUsername && !$existsEmail) {
			$shapass = sha1(strtolower($username).":".strtolower($pass));
			
			//insert
			$sql = "INSERT INTO users (email, username, password, creation_date, register_ip) VALUES
			($1, $2, $3, current_timestamp, $4);";
			$sqlName = 'insertNewUser';
			if (!pg_prepare ($sqlName, $sql))
				die("Can't prepare '$sql': " . pg_last_error());
			
			$result = pg_execute(self::$conn, $sqlName, array($email, $username, $shapass, $_SERVER['REMOTE_ADDR']));
			
			echo "success";
			return true;
		}
		else {
			return false;
		}
    }
	
	public static function CheckPasswordMatch($username, $pass) {
		$shapass = sha1(strtolower($username).":".strtolower($pass));
		
		$sql = 'SELECT user_id FROM users WHERE lower(username) = lower($1) AND lower(password) = lower($2)';
		$sqlName = 'checkPasswordMatch';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username, $shapass));
		
		$arr = pg_affected_rows($result);
		return $arr > 0;
	}

	public static function UpdatePassword($username, $newPassword) {
		
		$shapass = sha1(strtolower($username).":".strtolower($newPassword));
		
		$sql = 'UPDATE users SET password = lower($2) WHERE lower(username) = lower($1)';
		$sqlName = 'updatePassword';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username, $shapass));
	}
	
	public static function UpdateEmail($username, $newEmail) {
		
		$sql = 'UPDATE users SET email = lower($2) WHERE lower(username) = lower($1)';
		$sqlName = 'updateEmail';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username, $newEmail));
	}
	
	public static function GetEmail($username) {
		
		$sql = 'SELECT email FROM users WHERE lower(username) = lower($1)';
		$sqlName = 'getEmail';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username));
		
		$arr = pg_fetch_array($result);
		return $arr["email"];
	}
	
	public static function GetAccountData($username) {
		
		$sql = 'SELECT * FROM users WHERE lower(username) = lower($1)';
		$sqlName = 'getAccountData';
		if (!pg_prepare ($sqlName, $sql))
			die("Can't prepare '$sql': " . pg_last_error());
		
		$result = pg_execute(self::$conn, $sqlName, array($username));
		
		$arr = pg_fetch_array($result);
		return $arr;
	}
	
}


?>