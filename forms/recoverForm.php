<? 

if(isset($_POST['user'])) {
	
	//Generate new password
	$PASSWORD_LENGTH = 7;
	$newPassword = substr(str_shuffle(strtolower(sha1(rand() . time() . "tfg"))),0, $PASSWORD_LENGTH);
	
	include ("database.php");
	
	Database::Connect();
	
	//Check if the introduced user exists
	if(Database::CheckIfUsernameExists($_POST['user'])) {
		//Then, update with the new password
		Database::UpdatePassword($_POST['user'], $newPassword);
		
		//And send an email
		include ("mailer.php");
			
		Mailer::SendMail(Database::GetEmail($_POST['user']), ucfirst(strtolower($_POST['user'])),
			'Password recovery',
			"Hello, " . ucfirst(strtolower($_POST['user'])) . ". This is your new password: " . $newPassword,
			"Hello, " . ucfirst(strtolower($_POST['user'])) . ".<br>You requested a new password using the password recovery.<br><br>This is your new password: " . $newPassword . "<br><br>If you have not requested this operation, contact us replying to this message.<br><br>Have a nice day!"
		);
		echo "success";
	}
	else {
		echo "The user you introduced is not registered.";
	}
}
else
	echo "ERROR";
	
?>