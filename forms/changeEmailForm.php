<?

if(isset($_POST['newEmail']) && isset($_POST['pass'])) {
	
	include ("database.php");
	
	Database::Connect();
	
	//start php session
	session_start();
	
	//Check if the current password is correct
	if(Database::CheckPasswordMatch($_SESSION['user'], $_POST['pass'])) {
		
		//Send an email to the current direction to notify the change
		include ("mailer.php");
			
		Mailer::SendMail(Database::GetEmail($_SESSION['user']), ucfirst(strtolower($_SESSION['user'])),
			'Email changed (old)',
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ". Email change request success.",
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ".<br>You requested an email change from the management webpage.<br><br>This was your registered email before changing it. Your new email is: ".strtolower($_POST['newEmail'])."<br><br>This email is sent only as notification for the change.<br>If you have not requested this operation, contact us replying to this message.<br><br>Have a nice day!"
		);
		
		//Update the email
		Database::UpdateEmail($_SESSION['user'], $_POST['newEmail']);
		
		//And send an email to the new direction to notify the change
		Mailer::SendMail($_POST['newEmail'], ucfirst(strtolower($_SESSION['user'])),
			'Email changed (new)',
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ". Email change request success.",
			"Hello, " . ucfirst(strtolower($_SESSION['user'])) . ".<br>You requested an email change from the management webpage.<br><br>This is now your registered email.<br><br>If you have not requested this operation, contact us replying to this message.<br><br>Have a nice day!"
		);
		
		session_destroy();
		
		echo "success";
	}
	else {
		echo "The password is not correct.";
	}
}
else
	echo "ERROR";
	
?>