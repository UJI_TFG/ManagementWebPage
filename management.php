<html>
  <head>
    <title>TFG Index</title>
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="js/functionality.js"></script>
  </head>
  <body>
  <?
  session_start();
  if(!isset($_SESSION['user']))
	echo '<script>window.location.href = "/";</script>';
  ?>
    <div id="content">
      <div id="tabs">
        <div class="tab selected" id="dataTab">ACCOUNT</div><div class="tab selected" id="downloadTab">DOWNLOAD GAME</div><div class="tab" id="changePassTab">CHANGE PASSWORD</div><div class="tab" id="changeMailTab">CHANGE EMAIL</div><div class="tab" id="logoutTab">LOG OUT</div>
      </div>
      <div id="dynamicContainer">SWAP CONTENT</div>
    </div>
  </body>
</html>